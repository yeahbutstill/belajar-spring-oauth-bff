# Contoh Aplikasi dengan stack OAuth2 Spring #

Daftar aplikasi :

1. Authorization Server

    * menyimpan database username, password, dan permission tiap user
    * menyediakan fitur :

        * database user
        * database aplikasi client
        * login user
        * login aplikasi 
        * OIDC Authentication
            
            * Authorization Code
            * Client Credential
            * Device 

        * Token Endpoint
        * User Info Endpoint

2. Resource Server

    * berfungsi sebagai backend service
    * menyediakan endpoint:

        * daftar nasabah
        * daftar transaksi sesuai user yang sedang login

3. Confidential Client

    * menyediakan tampilan berbasis web untuk data yang disediakan resource server
    * mendelegasikan fungsi login ke authorization server
    * memanggil endpoint resource server untuk mengambil data transaksi
    * menampilkan data dalam format HTML, dirender di server side

4. Single Page Application (SPA) Client - Direct

    * contoh aplikasi client side
    * tidak bisa menyimpan client secret (karena source code terbaca oleh user)
    * menyimpan access token di Service Worker

5. Backend For Frontend (BFF)

    * menyediakan session management untuk SPA
    * interaksi dengan SPA menggunakan mekanisme session id dan cookie biasa
    * melakukan authorization code flow dengan authorization server
    * menyimpan access token database BFF
    * mencatat mapping antara session id dengan access token
    * menerima request dari SPA, mencarikan access token sesuai session id, dan meneruskan request ke resource server

6. Single Page Application (SPA) Client - via BFF

    * secara fungsi sama dengan aplikasi 4
    * tidak melihat apalagi menyimpan access token

7. Mobile Application

    * aplikasi mobile, dibuat dengan Flutter
    * versi android dan ios

## Setup Environment ##

Tambahkan baris berikut di `/etc/hosts` laptop/PC anda

```
127.0.0.1 auth-server
127.0.0.1 resource-server
127.0.0.1 client-app
127.0.0.1 js-app
127.0.0.1 bff-app
```

## Menjalankan Aplikasi ##

1. Jalankan Docker Compose

    ```
    docker compose up
    ```

2. Browse ke client-app : http://client-app:10002/mutasi/list?mulai=2021-01-01&sampai=2023-01-01

    * Username : `nasabah001`
    * Password : `teststaff`

3. Browse ke resource-server melalui BFF application : http://bff-app:10003/resource/api/transaksi/?mulai=2021-01-01&sampai=2021-01-02

4. Browse ke js-app melalui BFF : http://bff-app:10003/js-app/

# Referensi #

* [SPA Best Practices](https://curity.io/resources/learn/spa-best-practices/)
* [Berbagai opsi authentication pada SPA](https://youtu.be/OpFN6gmct8c?si=jP1VElIR6w4DF-Lf)
* [Panduan dari Google](https://developers.google.com/identity/protocols/oauth2)
* [Token Security di SPA](https://pragmaticwebsecurity.com/files/talks/tokensecurity.pdf)
* [OAuth2 dengan Flutter](https://tudip.com/blog-post/add-user-authentication-to-the-flutter-app-using-oauth-2-0/)
* [RFC OAuth untuk Native App](https://datatracker.ietf.org/doc/html/rfc8252)
* [Contoh Spring Cloud Gateway sebagai BFF](https://github.com/HQT-Team/bff-spring-keycloak-react-demo)
* [Konfigurasi Docker Compose untuk Aplikasi Spring dan Database](https://www.bezkoder.com/docker-compose-spring-boot-mysql)