# Aplikasi Authserver #

* Menjalankan database

    ```
    docker compose up
    ```

* Menjalankan aplikasi

   ```
   mvn clean spring-boot:run
   ```

* Connect ke database

    ```
    psql -h 127.0.0.1 -p 54321 -U authserver -d authserverdb
    ```

* Melihat isi tabel client

   ```
   select * from oauth2_registered_client;
   ```

## Authenticate dengan client credential ##

```
curl  -X POST \
  'http://localhost:10000/oauth2/token' \
  --header 'Accept: application/json' \
  --header 'Authorization: Basic YWRtaW4tY2xpZW50OmFkbWluLWNsaWVudDEyMw==' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=client_credentials' \
  --data-urlencode 'scope=user.read'
```

Hasilnya seperti ini

```json
{
    "access_token": "eyJraWQiOiIzZTBiODM0YS0xMzA5LTQ3ZGYtODk4NS05ZmMxOWUxZDQ0ZjciLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbi1jbGllbnQiLCJhdWQiOiJhZG1pbi1jbGllbnQiLCJuYmYiOjE2ODY2NDcwMDIsInNjb3BlIjpbInVzZXIucmVhZCJdLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAiLCJleHAiOjE2ODY2NDczMDIsImlhdCI6MTY4NjY0NzAwMn0.EkuwcbVpY0tk3mhPyzLmEbvxuUI7kuToLgR5tY79bWZ-mp8xHWVhMYJYJ4P2Yz7l7-zmyzIz7WEBgarNNLMgBtdV-Dn4h44msrCfHgABn1ZkLqHd8r_dmkxFWMrZvOdotTkZicpvnSUQbUYxpBoTjgiw2DMzZh_LyMjdEJMntvjqxYrqnWRQjI6pFaSVBNOVBHOK03peFweydpwBI4_aEyFYMOYL3jm_i0pZLGvCzbWT7emNDK2DM28GNRn5P1cd_xSCEVLn8ov6zs_sf3Sslrc1wuzcCvTXSDdz0HLVHlrOHzuEW1s4_eo0O9CrGwfcbOTEIyParPFQXllV8fI5IA",
    "scope": "user.read",
    "token_type": "Bearer",
    "expires_in": 299
}
```

Token introspection

```
curl  -X POST \
  'http://localhost:10000/oauth2/introspect' \
  --header 'Accept: application/json' \
  --header 'Authorization: Basic YWRtaW4tY2xpZW50OmFkbWluLWNsaWVudDEyMw==' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'token=eyJraWQiOiIzZTBiODM0YS0xMzA5LTQ3ZGYtODk4NS05ZmMxOWUxZDQ0ZjciLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbi1jbGllbnQiLCJhdWQiOiJhZG1pbi1jbGllbnQiLCJuYmYiOjE2ODY2NDcwMDIsInNjb3BlIjpbInVzZXIucmVhZCJdLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAiLCJleHAiOjE2ODY2NDczMDIsImlhdCI6MTY4NjY0NzAwMn0.EkuwcbVpY0tk3mhPyzLmEbvxuUI7kuToLgR5tY79bWZ-mp8xHWVhMYJYJ4P2Yz7l7-zmyzIz7WEBgarNNLMgBtdV-Dn4h44msrCfHgABn1ZkLqHd8r_dmkxFWMrZvOdotTkZicpvnSUQbUYxpBoTjgiw2DMzZh_LyMjdEJMntvjqxYrqnWRQjI6pFaSVBNOVBHOK03peFweydpwBI4_aEyFYMOYL3jm_i0pZLGvCzbWT7emNDK2DM28GNRn5P1cd_xSCEVLn8ov6zs_sf3Sslrc1wuzcCvTXSDdz0HLVHlrOHzuEW1s4_eo0O9CrGwfcbOTEIyParPFQXllV8fI5IA'
```

Hasilnya seperti ini

```json
{
    "active": true,
    "sub": "admin-client",
    "aud": [
        "admin-client"
    ],
    "nbf": 1686642274,
    "scope": "user.read",
    "iss": "http://localhost:10000",
    "exp": 1686642574,
    "iat": 1686642274,
    "client_id": "admin-client",
    "token_type": "Bearer"
}
```

## Authentication dengan Flow Authorization Code dengan PKCE ##

1. Cari tahu dulu URL yang digunakan untuk prosedur OAuth 2.0

    ```
    curl http://localhost:10000/.well-known/openid-configuration
    ```
   
    Hasilnya seperti ini

    ```json
    {
        "issuer": "http://localhost:10000",
        "authorization_endpoint": "http://localhost:10000/oauth2/authorize",
        "device_authorization_endpoint": "http://localhost:10000/oauth2/device_authorization",
        "token_endpoint": "http://localhost:10000/oauth2/token",
        "token_endpoint_auth_methods_supported": [
            "client_secret_basic",
            "client_secret_post",
            "client_secret_jwt",
            "private_key_jwt"
        ],
        "jwks_uri": "http://localhost:10000/oauth2/jwks",
        "userinfo_endpoint": "http://localhost:10000/userinfo",
        "end_session_endpoint": "http://localhost:10000/connect/logout",
        "response_types_supported": [
            "code"
        ],
        "grant_types_supported": [
            "authorization_code",
            "client_credentials",
            "refresh_token",
            "urn:ietf:params:oauth:grant-type:device_code"
        ],
        "revocation_endpoint": "http://localhost:10000/oauth2/revoke",
        "revocation_endpoint_auth_methods_supported": [
            "client_secret_basic",
            "client_secret_post",
            "client_secret_jwt",
            "private_key_jwt"
        ],
        "introspection_endpoint": "http://localhost:10000/oauth2/introspect",
        "introspection_endpoint_auth_methods_supported": [
            "client_secret_basic",
            "client_secret_post",
            "client_secret_jwt",
            "private_key_jwt"
        ],
        "subject_types_supported": [
            "public"
        ],
        "id_token_signing_alg_values_supported": [
            "RS256"
        ],
        "scopes_supported": [
            "openid"
        ]
    }
    ```

2. Generate PKCE

    * Code Verifier : 43 - 128 random string. Misal : `EmJ1jTS245HXMu5dDFc36XlEK02FCfT3BAvbvVfBiXSl`
    * Code Challenge : SHA256(code verifier). Misal : `ea3rEXbTCcvWGOL2m6J1lT2VWv-sLrnS2i-UeaNENbw`

3. Akses `authorization_endpoint` di [http://auth-server:10000/oauth2/authorize?client_id=confidential-client&redirect_uri=http://client-app:10002/login/oauth2/code/confidential-client&response_type=code&scope=openid&state=abcd1234&code_challenge_method=S256&code_challenge=ea3rEXbTCcvWGOL2m6J1lT2VWv-sLrnS2i-UeaNENbw](http://auth-server:10000/oauth2/authorize?client_id=confidential-client&redirect_uri=http://client-app:10002/login/oauth2/code/confidential-client&response_type=code&scope=openid&state=abcd1234&code_challenge_method=S256&code_challenge=ea3rEXbTCcvWGOL2m6J1lT2VWv-sLrnS2i-UeaNENbw). 
   
    
    Login dengan username `nasabah001` dan password `teststaff`


4. Dapatkan `authorization_code` setelah berhasil login, misalnya `IE_yzrqdjYxgKbxr1vIjYSAbjB-lDP_zc_cf4fX7rf-ObFbzRuY8UrM_h-X-MzrO2jVDVWr9fYr-d0XGdTw-35kFo2_6K5CXF9oXWFm_YuUIgw_R2Q43TDHLUqWzcobm`

    

5. Tukarkan `authorization_code` menjadi `access_token`

    [![Token Endpoint](img/token-endpoint.png)](img/token-endpoint.png)

    ```
    curl  -X POST \
        'http://auth-server:10000/oauth2/token' \
        --header 'Authorization: Basic Y29uZmlkZW50aWFsLWNsaWVudDpjb25maWRlbnRpYWwtY2xpZW50MTIz' \
        --header 'Content-Type: application/x-www-form-urlencoded' \
        --data-urlencode 'grant_type=authorization_code' \
        --data-urlencode 'redirect_uri=http://client-app:10002/login/oauth2/code/confidential-client' \
        --data-urlencode 'client_id=confidential-client' \
        --data-urlencode 'code=yNJ7QNFMvwJ9XG4Nk20_DDxtdwC4_dQfpHuTDtcojOuap_N0U2Ay7ml7Jdqqrxr7VMKzeh9WghCJvj48nSk5-NZB8fS_pdJB9hebl-I3X3oAS7utdJhE6hUd_xHXrtzT' \
        --data-urlencode 'code_verifier=EmJ1jTS245HXMu5dDFc36XlEK02FCfT3BAvbvVfBiXSl'
    ```
   
    Hasilnya seperti ini 
   
    ```json
    {
        "access_token": "eyJraWQiOiI3YjAyMzVkZC0xNGVmLTQ4MDItOTY3OC03MzdlNTIyM2YxMGYiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMDAxIiwiYXVkIjoiY29uZmlkZW50aWFsLWNsaWVudCIsIm5iZiI6MTY4OTY2NTc3OCwic2NvcGUiOlsib3BlbmlkIl0sImlzcyI6Imh0dHA6Ly9hdXRoLXNlcnZlcjoxMDAwMCIsImV4cCI6MTY4OTY2NjA3OCwiaWF0IjoxNjg5NjY1Nzc4fQ.ce9tTTRL0epXh3rG1vONzCYvWyEZZAhA9haPBz723b8hWn0dKEc6kLQkNTraNAKRYt0sKZPNQCzOke_LKMq3XeE_X0xxI9IWi2X7XBEc4QmXql_LoHRAvrGyEF9bA8iwZ5RIlk9bXSA9kFtByFdzXl-Cz9J-o49FXRHGExZj0KvM-_nQI7NG5GdyLLJ8XEHd2Nl2MkZxqT43F76o5sQTztUAAQsVHQoWDZWRbGA4HpQMrtugps1iaC9pCZD1EkxPYVUUqhzjGJbXx_fz0Hm24Hu3yAM9_CR1_hab8cu1SgMuQS9-RPbWBCjT-b3iUFp2Op8FRBH9AcfTbzB2ANvoPA",
        "refresh_token": "qg0Gh4pmW_PrFWcznLTr_Abn9M8DW18nXHBxxweENgKFoJzyrreDd127hxpfC0DC0UlnsucJ_FMo1lVEHhQPX6oHTEObCCH34AldQkx4T6NsyC1qMXOmINf9FyLTX0So",
        "scope": "openid",
        "id_token": "eyJraWQiOiI3YjAyMzVkZC0xNGVmLTQ4MDItOTY3OC03MzdlNTIyM2YxMGYiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMDAxIiwiYXVkIjoiY29uZmlkZW50aWFsLWNsaWVudCIsImF6cCI6ImNvbmZpZGVudGlhbC1jbGllbnQiLCJhdXRoX3RpbWUiOjE2ODk2NjU1ODksImlzcyI6Imh0dHA6Ly9hdXRoLXNlcnZlcjoxMDAwMCIsImV4cCI6MTY4OTY2NzU3OCwiaWF0IjoxNjg5NjY1Nzc4LCJzaWQiOiJDeHliS3lTZThtVUljbzNnaUtKbjV3dkdzOUFOVEdjcDdJYXBhS3lqRExZIn0.XpJtGCsG43tDdp5bk0qsFP-rNHZCtcYf-Ax88LV-b-pGE_qbFDq3ZveHQXBZl-ibCC8_47q-rk2vFc6_jP3UCpr9Lyjh0t-xyqhTVSA6E7Buc8lMq1yFVXDusOgYrtsPfI38k5wBdp-vnr8ISGWLG97MiFNN3pUVrdIrAhmXe0IVOgpIm7QSX3qN82GN-cTjbCKiR6DCAJ9I4xHDETG_3ncxHhC46N9CEkC9ivbc9dfXVjYni-h0h2INmitGBYHSTGkcYTa-joBuNA1_pAsCVdu2SvEmeKC-fc5sKVCcy8NOSIbfGm_TrlZeS7WuS-EuDXk2GCQpbvxtbwHWr5uYDA",
        "token_type": "Bearer",
        "expires_in": 299
    }
    ```
