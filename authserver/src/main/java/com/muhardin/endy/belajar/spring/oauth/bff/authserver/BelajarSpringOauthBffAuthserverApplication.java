package com.muhardin.endy.belajar.spring.oauth.bff.authserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarSpringOauthBffAuthserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarSpringOauthBffAuthserverApplication.class, args);
	}

}
