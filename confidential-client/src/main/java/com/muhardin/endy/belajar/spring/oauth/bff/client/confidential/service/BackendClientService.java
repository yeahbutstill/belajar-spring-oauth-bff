package com.muhardin.endy.belajar.spring.oauth.bff.client.confidential.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.muhardin.endy.belajar.spring.oauth.bff.client.confidential.dto.TransaksiDto;

@Service
public class BackendClientService {
    private static final String URI_LIST_TRANSAKSI = "/api/transaksi/";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private WebClient webClient;
    
    @Autowired 
    public BackendClientService(WebClient webClient) {
        this.webClient = webClient;
    }

    public List<TransaksiDto> daftarTransaksi(OAuth2AuthorizedClient authorizedClient, LocalDateTime mulai, LocalDateTime sampai){
        
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
            .path(URI_LIST_TRANSAKSI)
            .queryParam("mulai", FORMATTER.format(mulai))
            .queryParam("sampai", FORMATTER.format(sampai))
            .build())
        .attributes(ServletOAuth2AuthorizedClientExchangeFilterFunction.oauth2AuthorizedClient(authorizedClient))
        .retrieve()
        .bodyToMono(new ParameterizedTypeReference<List<TransaksiDto>>(){})
        .block();
    }
}
