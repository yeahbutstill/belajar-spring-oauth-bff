package com.muhardin.endy.belajar.spring.oauth.bff.client.confidential.dto;

public enum JenisTransaksi {
    SETOR_TUNAI,PEMBAYARAN,PEMBELIAN,TARIK_TUNAI
}
