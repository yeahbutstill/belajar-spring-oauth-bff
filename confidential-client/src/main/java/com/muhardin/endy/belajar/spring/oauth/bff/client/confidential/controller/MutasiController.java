package com.muhardin.endy.belajar.spring.oauth.bff.client.confidential.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.muhardin.endy.belajar.spring.oauth.bff.client.confidential.dto.TransaksiDto;
import com.muhardin.endy.belajar.spring.oauth.bff.client.confidential.service.BackendClientService;

import lombok.extern.slf4j.Slf4j;

@Controller @Slf4j
public class MutasiController {

    private BackendClientService backendClientService;

    @Autowired 
    public MutasiController(BackendClientService backendClientService) {
        this.backendClientService = backendClientService;
    }

    @GetMapping("/mutasi/list")
    public void daftarMutasi(Model model, 
                @RequestParam(required = false, value="mulai") LocalDate mulai, @RequestParam(required = false, value="sampai") LocalDate sampai,
                @RegisteredOAuth2AuthorizedClient("confidential-client") OAuth2AuthorizedClient authorizedClient){
        
        if(mulai == null) {
            mulai = LocalDate.now().minusMonths(1);
        }

        if(sampai == null) {
            sampai = LocalDate.now();
        }

        List<TransaksiDto> data = backendClientService
            .daftarTransaksi(authorizedClient, mulai.atStartOfDay(), sampai.plusDays(1).atStartOfDay());
        
        log.info("Mulai : {}", mulai);
        log.info("Sampai : {}", sampai);
        log.info("Jumlah data : {}", data.size());
        
        model.addAttribute("daftarTransaksi", data);
    }
}
