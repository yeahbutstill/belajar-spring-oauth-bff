package com.muhardin.endy.belajar.spring.oauth.bff.client.confidential.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class TransaksiDto {
    private String id;
    private LocalDateTime waktuTransaksi;
    private JenisTransaksi jenisTransaksi;
    private String keterangan;
    private BigDecimal nilaiTransaksi;
}
