package com.muhardin.endy.belajar.spring.oauth.bff.client.confidential;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarSpringOauthBffConfidentialClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarSpringOauthBffConfidentialClientApplication.class, args);
	}

}
