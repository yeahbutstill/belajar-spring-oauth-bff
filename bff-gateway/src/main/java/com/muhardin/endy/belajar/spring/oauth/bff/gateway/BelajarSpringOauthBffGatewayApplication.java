package com.muhardin.endy.belajar.spring.oauth.bff.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarSpringOauthBffGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarSpringOauthBffGatewayApplication.class, args);
	}

}
