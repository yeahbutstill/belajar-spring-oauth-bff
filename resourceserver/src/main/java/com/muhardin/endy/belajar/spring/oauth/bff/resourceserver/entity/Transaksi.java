package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity @Data
public class Transaksi {

    @Id 
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_nasabah")
    private Nasabah nasabah;

    @NotNull
    private LocalDateTime waktuTransaksi = LocalDateTime.now();

    @NotNull
    @Enumerated(EnumType.STRING)
    private JenisTransaksi jenisTransaksi;

    @NotNull @NotEmpty
    private String keterangan;

    @NotNull @Min(1)
    private BigDecimal nilaiTransaksi;
}
