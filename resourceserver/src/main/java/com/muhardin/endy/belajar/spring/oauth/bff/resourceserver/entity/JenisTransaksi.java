package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity;

public enum JenisTransaksi {
    SETOR_TUNAI,PEMBAYARAN,PEMBELIAN,TARIK_TUNAI
}
