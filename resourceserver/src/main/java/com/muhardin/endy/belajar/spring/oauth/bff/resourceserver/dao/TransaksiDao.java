package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity.Nasabah;
import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity.Transaksi;

public interface TransaksiDao extends JpaRepository<Transaksi, String> {
    List<Transaksi> findByNasabahOrderByWaktuTransaksi(Nasabah nasabah);
    List<Transaksi> findByNasabahAndWaktuTransaksiBetweenOrderByWaktuTransaksi(Nasabah nasabah, LocalDateTime mulai, LocalDateTime sampai);
}
