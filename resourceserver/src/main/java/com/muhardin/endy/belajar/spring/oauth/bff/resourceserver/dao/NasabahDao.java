package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity.Nasabah;

public interface NasabahDao extends JpaRepository<Nasabah, String> {
    Optional<Nasabah> findByUsername(String username);
}
