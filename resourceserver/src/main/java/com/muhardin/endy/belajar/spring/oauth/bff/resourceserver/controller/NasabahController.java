package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.dao.NasabahDao;
import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity.Nasabah;

@RestController
@RequestMapping("/api")
public class NasabahController {
    private NasabahDao nasabahDao;

    @Autowired 
    public NasabahController(NasabahDao nasabahDao) {
        this.nasabahDao = nasabahDao;
    }

    @GetMapping("/nasabah/")
    public Page<Nasabah> dataNasabah(Pageable pageable){
        return nasabahDao.findAll(pageable);
    }
}
