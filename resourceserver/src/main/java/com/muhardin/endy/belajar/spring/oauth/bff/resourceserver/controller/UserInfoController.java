package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInfoController {

    @GetMapping("/api/me")
    public Map<String, String> me(Authentication currentUser, @AuthenticationPrincipal Jwt jwt){
        Map<String, String> userinfo = new HashMap<>();
        userinfo.put("username", currentUser.getName());
        userinfo.put("authorities", currentUser.getAuthorities().toString());
        userinfo.put("subject", jwt.getClaimAsString("sub"));
        userinfo.put("email", jwt.getClaimAsString("email"));
        return userinfo;
    }
}
