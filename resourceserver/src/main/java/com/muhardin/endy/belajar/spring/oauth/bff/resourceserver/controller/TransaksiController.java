package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.dao.NasabahDao;
import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.dao.TransaksiDao;
import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity.Nasabah;
import com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.entity.Transaksi;

import lombok.extern.slf4j.Slf4j;

@RestController @Slf4j
@RequestMapping("/api")
public class TransaksiController {
    private NasabahDao nasabahDao;
    private TransaksiDao transaksiDao;

    @Autowired
    public TransaksiController(NasabahDao nasabahDao, TransaksiDao transaksiDao) {
        this.nasabahDao = nasabahDao;
        this.transaksiDao = transaksiDao;
    }

    @PreAuthorize("hasAuthority('VIEW_TRANSAKSI')")
    @GetMapping("/transaksi/")
    public List<Transaksi> findByNasabah(
            @RequestParam(required = false, value = "mulai") LocalDate mulai,
            @RequestParam(required = false, value = "sampai") LocalDate sampai,
            Authentication currentUser){
        
        log.info("Current user : {}", currentUser.getName());
        log.info("Authorities : {}", currentUser.getAuthorities());
        
        Optional<Nasabah> optNasabah = nasabahDao.findByUsername(currentUser.getName());
        List<Transaksi> data = new ArrayList<>();
        if(!optNasabah.isPresent()) {
            return data;
        }

        Nasabah nasabah = optNasabah.get();
        
        if(mulai != null) {
            if(sampai == null) {
                sampai = LocalDate.now();
            }
            data = transaksiDao.findByNasabahAndWaktuTransaksiBetweenOrderByWaktuTransaksi(nasabah, 
                mulai.atStartOfDay(), sampai.plusDays(1).atStartOfDay());
        } else {
            data = transaksiDao.findByNasabahOrderByWaktuTransaksi(nasabah);
        }

        log.info("Mulai : {}", mulai);
        log.info("Sampai : {}", sampai);
        log.info("Jumlah data : {}", data.size());

        return data;
    }
}
