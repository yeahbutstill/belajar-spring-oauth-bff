package com.muhardin.endy.belajar.spring.oauth.bff.resourceserver.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Configuration
public class WebConfig {
	@Bean
	public WebMvcConfigurer corsMappingConfigurer(CorsConfig corsConfig) {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(@NonNull CorsRegistry registry) {
				registry.addMapping("/api/**")
				.allowCredentials(true)
				.allowedOriginPatterns(corsConfig.getOrigins().toArray(new String[corsConfig.getOrigins().size()]))
				.allowedMethods(corsConfig.getMethods().toArray(new String[corsConfig.getMethods().size()]));
			}
		};
	}

	@Configuration
	@ConfigurationProperties(prefix = "cors")
	@AllArgsConstructor @Getter
	public static class CorsConfig {
		private final List<String> origins;
        private final List<String> methods;
	}
}
