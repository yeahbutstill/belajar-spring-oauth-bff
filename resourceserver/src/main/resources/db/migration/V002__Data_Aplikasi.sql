insert into nasabah (id, username, nama) values 
('n001', 'nasabah001', 'Nasabah 001'),
('n002', 'nasabah002', 'Nasabah 002'),
('n003', 'nasabah003', 'Nasabah 003');

insert into transaksi (id, id_nasabah, waktu_transaksi, jenis_transaksi, keterangan, nilai_transaksi) values 
('t101', 'n001', '2021-01-01 09:00:00', 'SETOR_TUNAI', 'Setoran di Cabang ABCD', 100000),
('t102', 'n001', '2021-01-02 10:00:00', 'PEMBAYARAN', 'Pembayaran Listrik', 25000),
('t103', 'n001', '2021-01-03 11:00:00', 'TARIK_TUNAI', 'Penarikan di ATM 107', 50000),
('t201', 'n002', '2021-02-01 13:00:00', 'SETOR_TUNAI', 'Setoran di Cabang XYZ', 2000000),
('t202', 'n002', '2021-02-07 17:00:00', 'PEMBELIAN', 'Pembelian di Indoapril', 250000);