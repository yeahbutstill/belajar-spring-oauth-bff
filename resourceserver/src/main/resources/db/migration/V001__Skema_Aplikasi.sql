create table nasabah (
    id varchar(36), 
    username varchar(50) not null,
    nama varchar(100) not null,
    primary key (id), 
    unique(username)
);

create table transaksi (
    id varchar(36),
    id_nasabah varchar(36) not null,
    waktu_transaksi timestamp not null, 
    jenis_transaksi varchar(50) not null,
    keterangan varchar(255) not null,
    nilai_transaksi decimal(19,2) not null,
    primary key (id), 
    foreign key (id_nasabah) references nasabah(id)
);