# Aplikasi Backend #

Cara menggunakan

1. Jalankan aplikasi

    ```
    mvn clean spring-boot:run
    ```

2. Dapatkan `access_token` dari aplikasi authserver sesuai instruksi di [README authserver](../authserver/README.md)

    * Login dengan username `nasabah001` dan password `teststaff`

3. Hit endpoint rekap transaksi dengan membawa Bearer Token

    [![Rekap Transaksi](img/rekap-transaksi.png)](img/rekap-transaksi.png)

    ```
    curl  -X GET \
    'http://resource-server:10001/api/transaksi/?mulai=2021-01-01&sampai=2021-01-02' \
    --header 'Authorization: Bearer eyJraWQiOiI3MTc4YmU5MS0zYWM2LTQzYmItYWM0ZC05MDk3N2RhYWU1YTkiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuYXNhYmFoMDAxIiwiYXVkIjoiY29uZmlkZW50aWFsLWNsaWVudCIsIm5iZiI6MTY4OTY3MTY4NSwic2NvcGUiOlsib3BlbmlkIl0sImlzcyI6Imh0dHA6Ly9hdXRoLXNlcnZlcjoxMDAwMCIsImV4cCI6MTY4OTY3MTk4NSwiaWF0IjoxNjg5NjcxNjg1LCJhdXRob3JpdGllcyI6WyJWSUVXX1RSQU5TQUtTSSJdfQ.pnPSIjVZ52wCd9DDjlja_9Ox4F_n6u_hCJCQCYEHVLD9NZSv8sc-kQ_hpoltDYK13sRhRAFsVC-tv-oQksCHNSwHW5I58NYw9fS0LLPVUbnLPBUme3SDNwXboL2K7X5DVAiuOkpfIar0ijQtgj36A63sYRpA4mdoUeh_HZhol1kIyn6pFQZkwwcqroBD_LpPrZoowgnDXfjaWTZBaMVrLOQ8GxdXVVEQNSF7OBtMUe7T3tQOEq_NUSrGqDAvgj0Bgpud0j5lvsFSWJCwWXoqk-YKzk0QzuSSfR-mQ3paN8T7B91aB8MdAz3VMVkIFbvcIf8tQ39Jj6seio0MyiJvhA'
    ```